export interface Project {
    project: string,
    start: string,
    end: string,
    info: string,
}

export const emptyProject = () : Project => ({
    project: "",
    start: "",
    end: "",
    info: "",
})