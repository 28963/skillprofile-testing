import { SkillProfile } from "./skillprofile";

export const PROFILE: SkillProfile = {
    accountInfo: {
        username: 's.scholzfabian@gmail.com',
        firstname: 'Fabian',
        surname: 'Scholz',
        birthday: '2001-06-17',
    },
    skillprofile : {
        
    general: {
        job : 'Software maker',
        company : 'Woop',
        nationality : 'Deutsch',
        email : 'professional-mail@gmail.com',
        phone : '0176 375693884',
        city : 'Krefeld'
    },
    keyStrengths : [
        'Programmieren',
        'Unity Spieleentwicklung, vorallem 2D Spiele'
    ],
    skills : [
        {
            skill : 'Unity',
            category : 'PC',
            rating : 5
        },
        {
            skill : 'MagickaVoxel',
            category : 'PC',
            rating : 3
        }
    ],
    projects : [
        {
            project : 'Burning Junglex',
            start : '12.01.2013',
            end : '19.02.2014',
            info : 'Much wow Much wow Much wow Much wowMuch wow Much wow Much wow Much wowMuch wowMuch wowMuch wowMuch wowMuch wowMuch wow Much wowMuch wow Much wow Much wow Much wow '
        },
        {
            project : 'Gold Emblem',
            start : '12.02.2015',
            end : '06.01.2016',
            info : 'The Goal was to create a game in unity. It should be 2D and a turn based game. Upon spawning, you are able to move in a grid with a given endurance. You, as a player, aim to collect as much gold as possible. There are roundabout 30 rounds. With each 10th round there is more gold on the map to collect. Additionally, there is a wandering merchant coming in after round 5. He sells different Items like a better pickaxe or a surfboard to cross water. The map is generated randomly. There is mostly water with some islands depending on the settings. These islands are connected with bridges if they meet a certain size.'
        },
        {
            project : 'Gold Emblem',
            start : '12.02.2015',
            end : '06.01.2016',
            info : 'The Goal was to create a game in unity. It should be 2D and a turn based game. Upon spawning, you are able to move in a grid with a given endurance. You, as a player, aim to collect as much gold as possible. There are roundabout 30 rounds. With each 10th round there is more gold on the map to collect. Additionally, there is a wandering merchant coming in after round 5. He sells different Items like a better pickaxe or a surfboard to cross water. The map is generated randomly. There is mostly water with some islands depending on the settings. These islands are connected with bridges if they meet a certain size.'
        },
        {
            project : 'Gold Emblem',
            start : '12.02.2015',
            end : '06.01.2016',
            info : 'The Goal was to create a game in unity. It should be 2D and a turn based game. Upon spawning, you are able to move in a grid with a given endurance. You, as a player, aim to collect as much gold as possible. There are roundabout 30 rounds. With each 10th round there is more gold on the map to collect. Additionally, there is a wandering merchant coming in after round 5. He sells different Items like a better pickaxe or a surfboard to cross water. The map is generated randomly. There is mostly water with some islands depending on the settings. These islands are connected with bridges if they meet a certain size.'
        }
    ],
    qualifications : [
        {
            start : '2012-08-12',
            end : '2020-06-26',
            place : 'Gymnasium Horkesgath',
            info : 'Abschluss: Hochschulreife'
        },
        {
            start : '2020-08-12',
            end : '2024-03-30',
            place : 'Hochschule Rhein-Waal',
            info : 'Abschluss: Bachelor of Science'
        },
        {
            start : '2012-08-12',
            end : '2020-06-26',
            place : 'Soost University',
            info : 'Abschluss: Bachelor of Art'
        }
    ]
    }
}