export interface Skill {
    skill: string,
    category: string,
    rating: number,
}

export const emptySkill = () : Skill => ({
    skill: "",
    category: "",
    rating: 1
})