export const ACCOUNTS: any = [
	{
		username: 'fapa@fapa',
		firstname: 'Fabian',
		surname: 'Schmabian',
		birthday: 'string',
		role: 'user'
	},
	{
		username: 'senneon@senneon',
		firstname: 'Nils',
		surname: 'Schmilz',
		birthday: 'string',
		role: 'admin'
	},
	{
		username: 'lasinon@lasinon',
		firstname: 'Simon',
		surname: 'Schminon',
		birthday: 'string',
		role: 'pm'
	},
	{
		username: 'pandara@pandara',
		firstname: 'Emily',
		surname: 'Schmemily',
		birthday: 'string',
		role: 'user'
	}
];
