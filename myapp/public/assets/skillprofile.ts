import { Qualification, emptyQalificarion } from "./qualification"
import { Skill, emptySkill } from "./skill"
import { Project, emptyProject } from "./project"
import { SkillprofileEditComponent } from "src/app/edit/skillprofile-edit/skillprofile-edit.component"

export interface SkillProfile {
    accountInfo: {
        username: string,
        firstname: string,
        surname: string,
        birthday: string,
    },
    skillprofile: {
        general: {
            job: string,
            company: string,
            nationality: string,
            email: string,
            phone: string,
            city: string,
        },
        keyStrengths: string[],
        skills: Skill[],
        projects: Project[],
        qualifications: Qualification[]
    }
}

export const emptySkillprofile = () : SkillProfile => ({
    accountInfo: {
        username: "",
        firstname: "",
        surname: "",
        birthday: "",
    },
    skillprofile: {
        general: {
            job: "",
            company: "",
            nationality: "",
            email: "",
            phone: "",
            city: "",
        },
        keyStrengths: [],
        skills: emptySkill() as unknown as Skill[],
        projects: emptyProject() as unknown as Project[],
        qualifications: emptyQalificarion as unknown as Qualification[]
    }
})
