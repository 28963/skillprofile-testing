/*
######
PREPARING + INTEGRATING MODULES
######
*/

//using express
const express = require("express");
const app = express();

app.use(express.static("public"));

app.use(function (req, res, next) {
    // Website, die sich verbinden darf: Angular WDS -- alternativ: '*' für alle Quellen
    res.setHeader("Access-Control-Allow-Origin", "http://localhost:4200");
    // Angabe der erlaubten HTTP Methoden
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
    // Angabe der erlaubten request Header
    res.header("Access-Control-Allow-Headers", "Content-Type, Authorization");
    // Request zur nächsten Schicht der Middleware weiterleiten
    next();
});

//using body-parser
var bodyParser = require("body-parser");
var jsonParser = bodyParser.json();

//using internal modules
const databaseControl = require("./databaseControl");
const init = require("./init.js");

//using bcryptjs
const bcrypt = require("bcryptjs");
const SECRET = "myPrivateKey12345";

//using passport
const passport = require("passport");
const BasicStrategy = require("passport-http").BasicStrategy;
const passportJwt = require("passport-jwt");
const JwtStrategy = passportJwt.Strategy;
const ExtractJwt = passportJwt.ExtractJwt;
const jwt = require("jsonwebtoken");

var pdf = require("pdfkit");

/*
######
Authentication
######
*/

// HTTP-Basic authentication with username / password
// the user's credentials are transmitted by the client in the
// HTTP Authorization header
passport.use(
    new BasicStrategy(async function (username, password, done) {
        //get data from Database
        let databaseResponse = await databaseControl.credentialsCheck({ username: username, password: password });

        if (databaseResponse.result) {
            console.log(`user ${username} logged in`);
            return done(null, databaseResponse);
        }
        // user doesn't exist or wrong password
        return done(null, false);
    })
);

// JWT Authentication with JSON Web Token
// the Authorization header must look like this: JWT xxxxx.yyyyy.zzzzz
// where xxxxx.yyyyy.zzzzz is the token string
passport.use(
    new JwtStrategy(
        {
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken("jwt"),
            secretOrKey: SECRET
        },
        async function (jwtPayload, done) {
            // the payload of the JWT has been extracted successfully by the extractor
            // note: the extractor checks if the token is expired. In this case, passport
            // sends 401 to the client and we don't reach this point in the code

            // do we have a user?
            if (await databaseControl.hasUser(jwtPayload.username)) {
                // we have a user, return it
                console.log(`user ${jwtPayload.username} was authenticated by a valid JWT`);
                return done(null, jwtPayload);
            }
            // user doesn't exist (the user may have been deleted after getting
            // a JWT that's still valid)
            console.log(`user ${jwtPayload.username} doesn't exist`);
            return done(null, false);
        }
    )
);

function createToken(user) {
    // create payload
    let payload = {
        username: user.username,
        role: user.role
    };

    // create the JWT and sign it
    let token = jwt.sign(payload, SECRET, {
        expiresIn: "90 days"
    });
    return token;
}

//check the user rights
async function checkUserRights(username, requiredRoles) {
    
    //get users role from database
    let userrole = await (await databaseControl.getAccount(username)).payload.role
    
    //check if the users role is under the requiredRoles
    for(i in requiredRoles){
        if(userrole === requiredRoles[i]){
            return true;
        }
    }
    return false;
}

/*
######
ROUTES
######
*/

//#####
//registration Routes
//#####

//einen user registrieren
app.post('/registration', jsonParser, async function (req, res) {
    //erstellt Accountobjekt
    let account = {
        username: req.body.username,
        firstname: req.body.firstname,
        surname: req.body.surname,
        birthday: req.body.birthday,
        passwordHash: bcrypt.hashSync(req.body.pwd, 10),
        role: "user"
    };

    //bereitet Datenbankanfrage vor und wartet auf Antwort
    let response = await databaseControl.addAccount(account);


    if (response.result && await checkUserRights(req.user.username, ["user","pm","admin"])) {
        //Wenn der Account erstellt wurde, soll der client einen gültigen JWT erhalten und somit direkt autorisiert
        res.status(201).json({ token: createToken(account) });;
    } else {
        //Wenn der Account nicht erstellt werden konnte, wird ein Fehler Code an den client gesendet
        //TODO Fehlercodes erweitern
        res.removeHeader('www-authenticate');
        res.status(401).send();
    }
});

app.get('/registration', jsonParser, passport.authenticate("jwt", { session: false }), async function (req, res) {
    //bereitet Datenbankanfrage vor und wartet auf Antwort
    let response = await databaseControl.getAccount(req.user.username);

    if (response.result && await checkUserRights(req.user.username, ["user","pm","admin"])) {
        //Wenn der Account erstellt wurde, soll der client einen gültigen JWT erhalten und somit direkt autorisiert
        res.status(200).json(response.payload);;
    } else {
        res.status(400).send();
    }
});

//#####
//registrations Routes
//#####

app.get('/registrations', jsonParser, passport.authenticate("jwt", { session: false }), async function (req, res) {
    //bereitet Datenbankanfrage vor und wartet auf Antwort
    let response = await databaseControl.getAllAccounts();

    if (response.result && await checkUserRights(req.user.username, ["admin"])) {
        //Wenn der Account erstellt wurde, soll der client einen gültigen JWT erhalten und somit direkt autorisiert
        res.status(200).json(response.payload);;
    } else {
        res.status(400).send();
    }
});

//#####
//registration/{username} Routes
//#####

app.put('/registration/:username', jsonParser, passport.authenticate("jwt", { session: false }), async function (req, res) {
    //bereitet Datenbankanfrage vor und wartet auf Antwort
    let response = await databaseControl.editAccount(req.body, req.params.username);

    if (response.result && await checkUserRights(req.user.username, ["user","pm","admin"])) {
        //Wenn der Account erstellt wurde, soll der client einen gültigen JWT erhalten und somit direkt autorisiert
        res.status(200).json();
    } else {
        res.status(400).send();
    }
});

app.get('/registration/:username', jsonParser, passport.authenticate("jwt", { session: false }), async function (req, res) {
    //bereitet Datenbankanfrage vor und wartet auf Antwort
    let response = await databaseControl.getAccount(req.params.username);

    if (response.result && await checkUserRights(req.user.username, ["user","pm","admin"])) {
        //Wenn der Account erstellt wurde, soll der client einen gültigen JWT erhalten und somit direkt autorisiert
        res.status(200).json(response.payload);;
    } else {
        res.status(400).send();
    }
});

app.delete('/registration/:username', jsonParser, passport.authenticate("jwt", { session: false }), async function (req, res) {
    //bereitet Datenbankanfrage vor und wartet auf Antwort
    let response = await databaseControl.deleteAccount(req.params.username);

    if (response.result && await checkUserRights(req.user.username, ["admin"])) {
        //Wenn der Account erstellt wurde, soll der client einen gültigen JWT erhalten und somit direkt autorisiert
        res.status(200).json();
    } else {
        res.status(400).send();
    }
});

//#####
//token Routes
//#####

//login
app.get(
    "/token",
    passport.authenticate("basic", { session: false, failWithError: true }),
    function (req, res) {
        // if execution reaches this point, the user was authenticated
        // sucessfully and the user object is stored in req.user

        // issue a JWT
        // for the structure see https://jwt.io/introduction/
        let credentials = {
            username: req.user.payload.username,
            role: req.user.payload.role
        };

        res.status(200).json({ token: createToken(credentials) });
    },
    (err, req, res, next) => {
        // error authenticating user
        // we have to prevent the browser from showing the Basic auth popup
        // and must thus remove the www-authenticate header from the response.
        // that's why we can't let passport send the 401, but have to send it
        // explicitly.
        res.removeHeader("www-authenticate");
        res.status(401).end();
    }
);

//#####
//skillprofile Routes
//#####

//Eigenes Profil Erstellen
app.post(
    "/skillprofile",
    jsonParser,
    passport.authenticate("jwt", { session: false }),
    async function (req, res) {
        //if authentiction reaches this point, the user was authenticated by valid jwt
        //call addProfil and pass request body and username
        let response = await databaseControl.addProfil(req.body, req.user.username);
        //if success, send 201 (created)
        if (response.result && await checkUserRights(req.user.username, ["user","pm","admin"])) {
            //if success, send 201 (created)
            res.status(201).send();
        } else {
            res.status(400).send(response.message);
        }
    }
);

//Eigenes Profile Bearbeiten
app.put('/skillprofile', jsonParser, passport.authenticate("jwt", { session: false }), async function (req, res) {
    //if authentiction reaches this point, the user was authenticated by valid jwt	
    //call addProfil and pass request body and username
    let response = await databaseControl.editSkillprofile(req.body, req.user.username);

    if (response.result && await checkUserRights(req.user.username, ["user","pm","admin"])) {
        res.status(200).send();
    } else if (response.message == "DOES_NOT_EXIST") {
        res.status(404).send(response.message)
    } else {
        res.status(400).send(response.message);
    }
});

//Eigenes Profil Erhalten
app.get(
    "/skillprofile",
    jsonParser,
    passport.authenticate("jwt", { session: false }),
    async function (req, res) {
        //if authentiction reaches this point, the user was authenticated by valid jwt
        console.log('URL ' + req.url);
        let response = await databaseControl.getSkillprofile(req.user.username); //call getUsersProfile with username

        if (response.result && await checkUserRights(req.user.username, ["user","pm","admin"])) {
            //if success
            res.status(200).json(response.payload); //response with users profile
        } else if (response.message == "DOES_NOT_EXIST") {
            res.status(404).send(response.message);
        } else {
            res.status(400).send(response.message);
        }
    }
);

//#####
//skillprofiles Routes
//#####

app.get(
    "/skillprofiles",
    jsonParser,
    passport.authenticate("jwt", { session: false }),
    async function (req, res) {
        //if authentiction reaches this point, the user was authenticated by valid jwt
        console.log('URL ' + req.url);
        let response = await databaseControl.getAllSkillprofiles(req.query);
        if (response.result && await checkUserRights(req.user.username, ["pm","admin"])) {//if success
            res.status(200).json(response.payload); //response with all profile
        } else {
            res.status(400).send(response.message);
        }
    }
);

//#####
//skillprofile/{username} Routes
//#####

//Skillprofil mit Usernamen
app.get(
    "/skillprofile/:username",
    jsonParser,
    passport.authenticate("jwt", { session: false }),
    async function (req, res) {
        //if authentiction reaches this point, the user was authenticated by valid jwt
        console.log('URL ' + req.url);
        let response = await databaseControl.getSkillprofile(req.params.username);

        if (response.result && await checkUserRights(req.user.username, ["pm","admin"])) {
            //if success
            res.status(200).json(response.payload); //response with users profile
        } else if (response.message == "DOES_NOT_EXIST") {
            res.status(404).send(response.message);
        } else {
            res.status(400).send(response.message);
        }
    }
);

app.delete(
    "/skillprofile/:username",
    jsonParser,
    passport.authenticate("jwt", { session: false }),
    async function (req, res) {
        //if authentiction reaches this point, the user was authenticated by valid jwt

        let response = await databaseControl.deleteSkillprofile(req.params.username);

        if (response.result && await checkUserRights(req.user.username, ["admin"])) {
            //if success
            res.status(200).json(response.payload); //response with users profile
        } else if (response.message == "DOES_NOT_EXIST") {
            res.status(404).send(response.message);
        } else {
            res.status(400).send(response.message);
        }
    }
);

//#####
//pdf/{username} Routes
//#####

app.get("/pdf/:username", jsonParser, async function (req, res) {
    const stream = res.writeHead(200, {
        "Content-Type": "application/pdf",
        "Content-Disposition": `attachment;filename=${req.params.username}.pdf`
    });
    await buildPDF(
        (chunk) => stream.write(chunk),
        () => stream.end(),
        req.params.username
    );
});

/*
######
SERVICES
######
*/

async function buildPDF(dataCallback, endCallback, username) {
    try {
        const doc = new pdf({ bufferPages: true });
        let tmp = await databaseControl.getSkillprofile(username);
        data = tmp.payload.pop();

        // Colors
        let headerColor = "#1d4d78";
        let defaultColor = "#000";
        let grey = "#a6a6a6";

        doc.on("data", dataCallback);
        doc.on("end", endCallback);

        // Titel
        doc
            .fontSize(20)
            .text(data.accountInfo.firstname + " " + data.accountInfo.surname + " - ", {
                continued: true
            })
            .fillColor(headerColor)
            .text(data.skillprofile.general.job);

        // General Information
        doc
            .fillColor(defaultColor)
            .fontSize(12)
            .text(
                "\nGeburtstag: " +
                data.accountInfo.birthday +
                "\nUnternehmen: " +
                data.skillprofile.general.company +
                "\nNationalität: " +
                data.skillprofile.general.nationality +
                "\nEmail: " +
                data.skillprofile.general.email +
                "\nTelefon: " +
                data.skillprofile.general.phone +
                "\nStandort: " +
                data.skillprofile.general.city
            );

        // Haupfähigkeiten
        doc.fillColor(headerColor).fontSize(16).text("\nHauptfähigkeiten\n\n", {
            align: "center"
        });

        doc.fillColor(defaultColor).fontSize(12).list(data.skillprofile.keyStrengths, {
            bulletRadius: 3
        });

        //Projects
        doc.fillColor(headerColor).fontSize(16).text("\nProjekte\n", {
            align: "center"
        });

        data.skillprofile.projects.forEach((element) => {
            doc
                .fillColor(defaultColor)
                .fontSize(14)
                .text("\n" + element.project, {
                    continued: true,
                    underline: true
                })
                .fillColor(grey)
                .text(" (" + element.start + " - " + element.end + ")\n", {
                    underline: false
                });

            doc
                .fillColor(defaultColor)
                .fontSize(12)
                .text(element.info + "\n");
        });

        //Skills
        doc.fillColor(headerColor).fontSize(16).text("\nSkills\n\n", {
            align: "center"
        });

        data.skillprofile.skills.forEach((element) => {
            doc
                .fillColor(defaultColor)
                .fontSize(12)
                .text(element.skill + ": " + element.rating);
        });

        //Qualifications
        doc.fillColor(headerColor).fontSize(16).text("\nQualifikationen\n\n", {
            align: "center"
        });

        data.skillprofile.qualifications.forEach((element) => {
            doc
                .fillColor(defaultColor)
                .fontSize(14)
                .text(element.place, {
                    continued: true,
                    underline: true
                })
                .fillColor(grey)
                .text(" (" + element.start + " - " + element.end + ")\n", {
                    underline: false
                });

            doc
                .fillColor(defaultColor)
                .fontSize(12)
                .text(element.info + "\n");
        });

        // Print of current day
        let today = new Date();
        let dd = String(today.getDate()).padStart(2, "0");
        let mm = String(today.getMonth()).padStart(2, "0");
        let yy = today.getFullYear();

        let datePrint = dd + "." + mm + "." + yy;

        doc
            .fillColor(defaultColor)
            .fontSize(8)
            .text("\nDas Profil wurde am " + datePrint + " exportiert.");

        doc.end();
    } catch (err) {
        console.log("PDF Export: " + err.code);
    }
}

/*
######
GENERAL FUNCTIONS
######
*/


/*
######
STARTING WEBSERVER
######
*/

app.listen(3000, () => {
    console.log("Server online");

    //Create Dummy Data
    init.initDummy();
});