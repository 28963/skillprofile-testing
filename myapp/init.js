//using internal modules
const databaseControl = require("./databaseControl");

//using bcryptjs
const bcrypt = require("bcryptjs");
const SECRET = "myPrivateKey12345";

//using fs
const fs = require("fs");

const path = "./dummyData/";

exports.initDummy = async function(){
    console.log("###CREATE DUMMY START###");

    await createDummy("MatsHummels");
    await createDummy("MaxVerstappen");
    await createDummy("DarthVader");
    await createDummy("GandalfDergraue");
    await createDummy("Admin");
    await createDummy("ProjectManager");

    console.log("###CREATE DUMMY End###");
}

async function createDummy(name){
    console.log("CREATE " + name);
    let account;
    let profile

    try{
        account = JSON.parse(fs.readFileSync(path + name + "Account.json", 'utf8'));
        account.passwordHash = bcrypt.hashSync("Passwort", 10);
        await databaseControl.addAccount(account);
    }catch{
        //Nichts passiert...
    }

    try{
        profile = JSON.parse(fs.readFileSync(path + name + "Profile.json", 'utf8'));
        await databaseControl.addProfil(profile, account.username);
    }catch{
        //Nichts passiert...
    }
}