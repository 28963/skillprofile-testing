import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';
import { Project } from 'src/assets/project';

@Component({
  selector: 'app-projectinfo-edit',
  templateUrl: './projectinfo-edit.component.html',
  styleUrls: ['./projectinfo-edit.component.css']
})
export class ProjectinfoEditComponent implements OnInit {

  projectsForm!: FormGroup;
  @Output() outputFromChild : EventEmitter<any> = new EventEmitter();

  @Input() projectlist!: Project[];

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.projectsForm = this.fb.group({
      projects: this.fb.array([])
    })

    // lädt vorhandende Daten in die Form
    this.loadProjects();

    // Wenn noch keine Projekte angelegt/ reingeladen wurden soll ein leeres Inputfeld generiert werden
    if(this.projects.length == 0) {
      this.addProject("");
    }
  }

  get projects(): FormArray{
    return this.projectsForm.get("projects") as FormArray;
  }

  newProject(project:any): FormGroup{
    return this.fb.group({
      project: project.project,
      start: project.start,
      end: project.end,
      info: project.info
    })
  }

  // wird einmal beim initialisieren ausgeführt und beim klicken des Plusbuttons
  addProject(project:any) {
    this.projects.push(this.newProject(project));
  }

  // onInit
  loadProjects() {
    for (var i = 0; i < this.projectlist.length; i++) {
      this.addProject(this.projectlist[i]);
    }
  }

  // wird beim klicken des Mülleimerbuttons ausgeführt
  removeProject(index: number){
    this.projects.removeAt(index);
  }

  // Output for skillprofile-edit-component
  sendDataToSkillprofileEdit() {
    this.outputFromChild.emit(this.projectsForm.value);
  }
}
