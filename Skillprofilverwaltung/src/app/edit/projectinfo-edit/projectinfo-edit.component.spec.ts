import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectinfoEditComponent } from './projectinfo-edit.component';

describe('ProjectinfoEditComponent', () => {
  let component: ProjectinfoEditComponent;
  let fixture: ComponentFixture<ProjectinfoEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectinfoEditComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProjectinfoEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
