import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyStrengthsinfoEditComponent } from './key-strengthsinfo-edit.component';

describe('KeyStrengthsinfoEditComponent', () => {
  let component: KeyStrengthsinfoEditComponent;
  let fixture: ComponentFixture<KeyStrengthsinfoEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KeyStrengthsinfoEditComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(KeyStrengthsinfoEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
