import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';

@Component({
  selector: 'app-key-strengthsinfo-edit',
  templateUrl: './key-strengthsinfo-edit.component.html',
  styleUrls: ['./key-strengthsinfo-edit.component.css']
})
export class KeyStrengthsinfoEditComponent implements OnInit {

  keystrengthsinfoForm!: FormGroup;
  @Output() outputFromChild : EventEmitter<any> = new EventEmitter();

  @Input() keyStrengths!: string[];

  private addButton!: HTMLElement;

  removeBtnCondition: boolean = false;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.keystrengthsinfoForm = this.fb.group({
      strengths: this.fb.array([])
    })
    // lädt vorhandende Daten in die Form
    this.loadStrenghts();
    
    // Wenn noch keine Hauptfähigkeiten angelegt/ reingeladen wurden soll ein leeres Inputfeld generiert werden
    if(this.strengths.length == 0) {
      this.addStrength("");
    }
    
    // get addButton
    this.addButton = document.getElementById("addStrengthBtn") as HTMLElement;
  }

  get strengths(): FormArray{
    return this.keystrengthsinfoForm.get("strengths") as FormArray;
  }

  newStrength(): FormGroup{
    return this.fb.group({
      strength: ""
    })
  }

  newStrengthWValue(value: any): FormGroup{
    return this.fb.group({
      strength: value
    })
  }

  // wird einmal beim initialisieren ausgeführt und beim klicken des Plusbuttons
  addStrength(value : any) {
    if (this.strengths.length < 5) {
      this.strengths.push(this.newStrengthWValue(value));
    } else  if (this.strengths.length == 4) {
      this.addButton.setAttribute("disabled", "");
    }
  }

  // onInit
  loadStrenghts() {
    for (var i = 0; i < this.keyStrengths.length; i++) {
      this.addStrength(this.keyStrengths[i]);
      //console.log("log this.keyStrengths " + i);console.log(this.keyStrengths[i]);
    }
  }

  // wird beim klicken des Mülleimerbuttons ausgeführt
  removeStrength(index: number){
    this.strengths.removeAt(index);
  }

  // Output for skillprofile-edit-component
  sendDataToSkillprofileEdit() {
    this.outputFromChild.emit(this.keystrengthsinfoForm.value);
  }

  checkRemoveBtnCondition(i: Number) :boolean {
    if (i == 0) {
      return false;
    }
    return true;
  }
}
