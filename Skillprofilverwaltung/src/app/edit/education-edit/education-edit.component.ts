import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';
import { Qualification } from 'src/assets/qualification';

@Component({
  selector: 'app-education-edit',
  templateUrl: './education-edit.component.html',
  styleUrls: ['./education-edit.component.css']
})
export class EducationEditComponent implements OnInit {

  educationsForm!: FormGroup;
  @Output() outputFromChild : EventEmitter<any> = new EventEmitter();

  @Input() qualifications!: Qualification[];

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.educationsForm = this.fb.group({
      educations: this.fb.array([])
    })

    // lädt vorhandende Daten in die Form
    this.loadEducation();

    // Wenn noch keine Einträge angelegt/ reingeladen wurden soll ein leeres Inputfeld generiert werden
    if(this.educations.length == 0) {
      this.addEducation("");
    }
  }

  get educations(): FormArray{
    return this.educationsForm.get("educations") as FormArray;
  }

  newEducation(education:any): FormGroup{
    return this.fb.group({
      start: education.start,
      end: education.end,
      place : education.place,
      info: education.info
    })
  }
  //wird einmal beim initialisieren ausgeführt und beim klicken des Plusbuttons
  addEducation(education:any) {
    this.educations.push(this.newEducation(education));
  }

  // onInit
  loadEducation() {
    for (var i = 0; i < this.qualifications.length; i++) {
      this.addEducation(this.qualifications[i]);
    }
  }

  // wird beim klicken des Mülleimerbuttons ausgeführt
  removeEducation(index: number){
    this.educations.removeAt(index);
  }

  // Output for skillprofile-edit-component
  sendDataToSkillprofileEdit() {
    this.outputFromChild.emit(this.educationsForm.value);
  }
}
