import { Component, OnInit, Output, Input, EventEmitter, ɵgetUnknownElementStrictMode } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, FormControl } from '@angular/forms';
import { Skill } from 'src/assets/skill';

@Component({
  selector: 'app-technology-methodeinfo-edit',
  templateUrl: './technology-methodeinfo-edit.component.html',
  styleUrls: ['./technology-methodeinfo-edit.component.css']
})
export class TechnologyMethodeinfoEditComponent implements OnInit {

  skillsForm!: FormGroup;
  ratingValue: Number = 1;
  @Output() outputFromChild : EventEmitter<any> = new EventEmitter();

  @Input() skillslist!: Skill[];

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.skillsForm = this.fb.group({
      skills: this.fb.array([])
    })

    // lädt vorhandende Daten in die Form
    this.loadSkills();

    // Wenn noch keine Projekte angelegt/ reingeladen wurden soll ein leeres Inputfeld generiert werden
    if(this.skills.length == 0) {
      this.addSkill("");
    }
  }

  ngAfterViewInit(): void {
    for (var i = 0; i < this.skillslist.length; i++) {
      this.loadRating(this.skillslist[i].rating, i);
    }
  }

  get skills(): FormArray{
    return this.skillsForm.get("skills") as FormArray;
  }

  newSkill(skill:any): FormGroup{
    var rating : string = "1";
    if (skill.rating !== undefined) {
      rating = skill.rating;
    }
    return this.fb.group({
      skill: skill.skill,
      rating: rating,
      //timeperiod: ""
    })
  }

  // wird einmal beim initialisieren ausgeführt und beim klicken des Plusbuttons
  addSkill(skill:any) {
    this.skills.push(this.newSkill(skill));
  }

  // onInit
  loadSkills() {
    for (var i = 0; i < this.skillslist.length; i++) {
      this.addSkill(this.skillslist[i]);
    }
  }
  
  // wird beim klicken des Mülleimerbuttons ausgeführt
  removeSkill(index: number){
    this.skills.removeAt(index);
  }

  // Output for skillprofile-edit-component
  sendDataToSkillprofileEdit() {
    this.outputFromChild.emit(this.skillsForm.value);
  }

  // Verändert die Value Anzeige für das jeweilige Rating der einzelnen Skills/ Qualifikationen
  onRatingChange($event : any) {
    // id des Input Elements das verändert wurde
    var elId = $event.source._elementRef.nativeElement.id;
    // HTML Element der Anzeige des Inputs
    const inputValueB = document.getElementById("ratingValue" + elId) as HTMLElement;
    // Updaten der Anzeige mit dem Value über das übermittelte Event (veränderung des Sliders)
    inputValueB.innerHTML = " " + $event.value + " ";
  }

  loadRating(rating : any, i : number){
    const inputValueB = document.getElementById("ratingValue" + i) as HTMLElement;
    inputValueB.innerHTML = " " + rating + " ";
  }
}
