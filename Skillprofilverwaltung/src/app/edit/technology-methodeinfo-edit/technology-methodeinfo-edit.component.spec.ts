import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnologyMethodeinfoEditComponent } from './technology-methodeinfo-edit.component';

describe('TechnologyMethodeinfoEditComponent', () => {
  let component: TechnologyMethodeinfoEditComponent;
  let fixture: ComponentFixture<TechnologyMethodeinfoEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TechnologyMethodeinfoEditComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TechnologyMethodeinfoEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
