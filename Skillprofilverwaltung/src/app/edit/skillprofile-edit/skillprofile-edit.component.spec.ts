import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillprofileEditComponent } from './skillprofile-edit.component';

describe('SkillprofileEditComponent', () => {
  let component: SkillprofileEditComponent;
  let fixture: ComponentFixture<SkillprofileEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SkillprofileEditComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SkillprofileEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
