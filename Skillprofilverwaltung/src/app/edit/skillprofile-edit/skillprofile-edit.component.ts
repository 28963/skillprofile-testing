import { Component, OnInit, Input, Output, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ApiService } from 'src/app/api.service';
import { Router } from '@angular/router';
import { SkillProfile, emptySkillprofile } from 'src/assets/skillprofile';
import { PROFILE } from 'src/assets/data';

import {ContactinfoEditComponent} from '../contactinfo-edit/contactinfo-edit.component';

@Component({
	selector: 'app-skillprofile-edit',
	templateUrl: './skillprofile-edit.component.html',
	styleUrls: ['./skillprofile-edit.component.css']
})
export class SkillprofileEditComponent implements OnInit {
	@ViewChild('contactinfo') contactinfo!: ContactinfoEditComponent;
	@ViewChild('keystrengthsinfo') keystrengthsinfo!: ContactinfoEditComponent;
	@ViewChild('projectsinfo') projectsinfo!: ContactinfoEditComponent;
	@ViewChild('skillsinfo') skillsinfo!: ContactinfoEditComponent;
	@ViewChild('educationsinfo') educationsinfo!: ContactinfoEditComponent;

	// Alle Daten der Form für den POST request
	completeFormData: any = {
		general: {},
		keyStrengths: {},
		skills: {},
		projects: {},
		qualifications: {}
	};

  profileExists: boolean = false;

  constructor(
    private apiService: ApiService,
    private router: Router) { }

  ngOnInit(): void {
    // Accountinfo um username zu bekommen
    this.getUsername();

    // getSkillprofile, um die Form zum bearbeiten eines vorhandenen Profils zu füllen
    
    this.apiService.getSkillprofile("")
      .subscribe({
        complete: () => { console.log("Profile obtained") },
        error: (error) => {
          if (error.status === 404) {
            console.log('kein Profil vorhanden');
            this.profileExists = false;
          } else {
            console.log('different error code');
          }
          this.profile = emptySkillprofile();
        },
        next: (response) => {
          console.log(response)
          this.profile = this.apiService.parseSkillProfile(response as unknown as string)[0];
          this.profileExists = true;
        }
      });
    /* this.profile = PROFILE;*/
  }
  
  profile!: SkillProfile;
  username!: String;

	// Hier werden die Daten der Form in "completeFormData" eingefügt und zum senden vorbereitet
	receiveFormdData(data: any) {
		//console.log(Object.keys(data)[0]);
		switch (Object.keys(data)[0]) {
			case 'job':
				this.completeFormData.general = data;
				break;
			case 'strengths':
				this.completeFormData.keyStrengths = this.formatStremgthsData(data.strengths);
				break;
			case 'projects':
				this.completeFormData.projects = data.projects;
				break;
			case 'skills':
				this.completeFormData.skills = data.skills;
				break;
			case 'educations':
				this.completeFormData.qualifications = data.educations;
				break;
			default:
				console.log(`? - Kenne ich nicht!`);
		}
	}
  
  onSubmit() {
    // Validierungscheck
    if(this.contactinfo.sendValToSkillprofileEdit()) {
      alert(this.contactinfo.sendValToSkillprofileEdit());
    } else {
      // Holen der Formdaten aus den anderen Komponenten
      this.getFormdata();
      
      console.log(this.completeFormData);

      // Senden der Formdata an den Server
      // Wenn noch keins angelegt wurde post
      if ( this.profileExists == false) {
        this.apiService.createSkillprofile(this.completeFormData)
          .subscribe({
            complete: () => {console.log("done")},
            error: (error) => {
              if (error.status !== undefined && error.status === 401) {
                console.log('Unauthorized');
              } else {
                console.log('Authorized createSkillprofile');
              }
            },
            next: (response) => {
              console.log(response);
              this.router.navigate(['/skillprofilePreview', this.username])
            }
        });
      } else {  // Wenn schon eins existiert put
        this.apiService.editSkillprofile(this.completeFormData)
          .subscribe({
            complete: () => {console.log("done")},
            error: (error) => {
              if (error.status !== undefined && error.status === 401) {
                console.log('Unauthorized');
              } else {
                console.log('Authorized editSkillprofile');
              }
            },
            next: (response) => {
              console.log(response);
              this.router.navigate(['/skillprofilePreview', this.username])
            }
        });
      }
    }  
  }

  // Passende Formatierung der Keytrengths Daten zur Verwendung in der Datenbank
  formatStremgthsData(strengths : any) :string[]{
    var strengthsArray = [];
    for (var i=0; i < strengths.length; i++) {
      strengthsArray.push(strengths[i].strength);
    }
    return strengthsArray;
  }
  
  // Holen der Formdaten aus den anderen Komponenten
  getFormdata() {
    this.contactinfo.sendDataToSkillprofileEdit();
    this.keystrengthsinfo.sendDataToSkillprofileEdit();
    this.projectsinfo.sendDataToSkillprofileEdit();
    this.skillsinfo.sendDataToSkillprofileEdit();
    this.educationsinfo.sendDataToSkillprofileEdit();
  }

  // Holt die allgemeinen Nutzerdaten. Wird Beispielsweise für die Weiterleitung auf profilepreview verwendet
  getUsername() {
    this.apiService.getAccountinfo()
      .subscribe({
        complete: () => { console.log("Accountinfo obtained") },
        error: (error) => {
          console.log('Error ' + error);
        },
        next: (response) => {
          console.log(response.username)
          this.username = response.username;
        }
      });
  }

}
