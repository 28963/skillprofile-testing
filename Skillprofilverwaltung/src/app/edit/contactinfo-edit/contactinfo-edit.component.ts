import { getLocalePluralCase } from '@angular/common';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-contactinfo-edit',
  templateUrl: './contactinfo-edit.component.html',
  styleUrls: ['./contactinfo-edit.component.css']
})

export class ContactinfoEditComponent implements OnInit {

  contactinfoForm!: FormGroup;
  @Output() outputFromChild : EventEmitter<any> = new EventEmitter();

  @Input() info!: any;
  @Input() personalData!: any;

  // Validator Patterns
  emailPattern = new RegExp("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$");
  phonePattern =  new RegExp("(\\+[0-9]{2})[ ]?\\d{5,15}");

  testName = "Benson";
  testVorname = "Luca";

  constructor(private fb: FormBuilder) { }
  
  ngOnInit(): void {
    this.contactinfoForm = this.fb.group({
      job: [this.info.job], 
      company: [this.info.company], 
      city: [this.info.city], 
      birthday: [this.info.birthday], 
      nationality: [this.info.nationality], 
      email: [this.info.email, [
        Validators.pattern(this.emailPattern)]], 
      phone: [this.info.phone, [
        Validators.pattern(this.phonePattern)]]
    })

    // get Name und Vorname für Bildplatzhalter plus Erstellung
    // this.generatePicPlaceholder();
  }

  // Output for skillprofile-edit-component
  sendDataToSkillprofileEdit() {
    this.outputFromChild.emit(this.contactinfoForm.value);
  }
  sendValToSkillprofileEdit() :string{
    return this.isFormValid();
  }

  // Getter
  get email(): FormControl{
    return this.contactinfoForm.get("email") as FormControl;
  }
  get phone(): FormControl{
    return this.contactinfoForm.get("phone") as FormControl;
  }

  // Hiffunktionen
  generatePicPlaceholder() {
    const picPlaceholder = document.getElementById("picPlaceholder") as HTMLElement;
    // get names
    var fcName = this.testName.charAt(0);
    var fcVorname = this.testVorname.charAt(0);
    picPlaceholder.innerHTML = fcVorname + fcName;
  }

  isFormValid() :string{
    var resp = "Ihr Profil kann so nicht gesendet werden! Prüfen Sie folgende Einträge auf Fehler: ";
    if (!this.email.hasError('pattern') && !this.phone.hasError('pattern')) {
	    return "";
    }

    if (this.email.hasError('pattern')) {resp = resp + " - E-Mail "}
	  if (this.phone.hasError('pattern')) {resp = resp + " - Telefonnummer "}

    return resp;
  }
}

