import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactinfoEditComponent } from './contactinfo-edit.component';

describe('ContactinfoEditComponent', () => {
  let component: ContactinfoEditComponent;
  let fixture: ComponentFixture<ContactinfoEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContactinfoEditComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ContactinfoEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
