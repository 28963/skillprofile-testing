import {Injectable, isDevMode} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {LocalStorageService} from './local-storage.service';
import {Observable} from 'rxjs';
import {ObserversModule} from '@angular/cdk/observers';
import {SkillProfile} from 'src/assets/skillprofile';

@Injectable({
	providedIn: 'root'
})
export class ApiService {
	constructor(private http: HttpClient, private localStorage: LocalStorageService) {}

	// Variables needed to build the url
	baseUrl = 'http://localhost:3000/';

	// Als Header für alle Server anfragen anhängen
	// JWT Token wird als Bearer Token mitgeliefert
	private getHeader(): HttpHeaders {
		return new HttpHeaders({Authorization: 'Bearer ' + this.localStorage.get('token')});
	}

	// Entfernt den JWT, wodurch Anfragen fehlschlagen --> User ist ausgeloggt
	logout() {
		this.localStorage.remove('token');
	}

	// Registerdaten des Users werden als JSON Objekt mitgeliefert (Format in der API), als Antwort kommt ein JWT (Verabeitung findet in der register-component.ts statt)
	registerUser(user: any): Observable<any> {
		return this.http.post<any>(`${this.baseUrl}registration`, user);
	}

	// Logindaten werden als Basic Auth mitgesendet, als Antwort kommt ein JWT (Verabeitung findet in der login-component.ts statt)
	loginUser(username: string, password: string) {
		let authInfo = 'Basic ' + window.btoa(username + ':' + password);
		return this.http.get<any>(`${this.baseUrl}token`, {
			headers: new HttpHeaders().set('Authorization', authInfo)
		});
	}

	// Formdaten des Profils und der Header mit dem JWT gesendet für die Profil erfassung in der Datenbank
	createSkillprofile(data: any): Observable<any> {
		return this.http.post<any>(`${this.baseUrl}skillprofile`, data, {headers: this.getHeader()});
	}

	editSkillprofile(data: any): Observable<any> {
		return this.http.put<any>(`${this.baseUrl}skillprofile`, data, {headers: this.getHeader()});
	}

	getPDF(username: string): Observable<any> {
		let headers = this.getHeader();
		headers.set('Accept', 'application/pdf');
		return this.http.get(`${this.baseUrl}pdf/${username}`, {headers: headers, responseType: 'blob'});
	}

	getSkillprofile(username: string): Observable<SkillProfile> {
		return this.http.get<any>(`${this.baseUrl}skillprofile/${username}`, {
			headers: this.getHeader()
		});
	}

	getAllSkillprofiles(queryString: string): Observable<any> {
		console.log('API url will be:');
		console.log(`${this.baseUrl}${queryString}`);
		return this.http.get<any>(`${this.baseUrl}skillprofiles${queryString}`, {headers: this.getHeader()});
	}

	parseSkillProfile(profileListJson: string): SkillProfile[] {
		let profileArray: SkillProfile[] = [];
		JSON.parse(JSON.stringify(profileListJson)).forEach((profile: SkillProfile) => {
			profileArray.push(profile);
		});
		return profileArray;
	}

	getAccountinfo(): Observable<any> {
		return this.http.get<any>(`${this.baseUrl}registration`, {headers: this.getHeader()});
	}

	getAllAcountinfos(): Observable<any> {
		return this.http.get<any>(`${this.baseUrl}registrations`, {headers: this.getHeader()});
	}

	editAccountInfo(accountInfo: any): Observable<any> {
		return this.http.put<any>(`${this.baseUrl}registration/${accountInfo.username}`, accountInfo, {headers: this.getHeader()});
	}
}
