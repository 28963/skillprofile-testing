import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfAssessmentScaleComponent } from './self-assessment-scale.component';

describe('SelfAssessmentScaleComponent', () => {
  let component: SelfAssessmentScaleComponent;
  let fixture: ComponentFixture<SelfAssessmentScaleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelfAssessmentScaleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SelfAssessmentScaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
