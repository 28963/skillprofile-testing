import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ApiService} from '../api.service';
import {LocalStorageService} from '../local-storage.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	userLogin = {
		mail: '',
		password: ''
	};

	// Boolean für Error-Message
	failedLogin = false;

	loginForm!: FormGroup;

	constructor(
		private router: Router,
		private apiService: ApiService,
		private localStorageService: LocalStorageService
	) {}

	ngOnInit(): void {
		this.loginForm = new FormGroup({
			mail: new FormControl(this.userLogin.mail, Validators.required),
			password: new FormControl(this.userLogin.password, [Validators.required])
		});
	}

	// Getter für die Loginform Controls
	get mail() {
		return this.loginForm.get('mail')!;
	}
	get password() {
		return this.loginForm.get('password')!;
	}

	onSubmit(): void {
		this.getFormValues();
		this.apiService.loginUser(this.userLogin.mail, this.userLogin.password).subscribe({
			complete: () => {
				console.log('Der Nutzer ist eingeloggt.');
			},
			error: (error) => {
				if (error.status !== undefined && error.status === 401) {
					console.log('Ungültige Anmeldedaten!');
					this.failedLogin = true;
					this.password.setValue('');
				} else {
					console.log('different error code');
				}
			},
			next: (response) => {
				this.localStorageService.set('token', response.token);
				console.log('Token wurde im LocalStorage gespeichert.');
				this.router.navigate(['/skillprofilePreview', '']);
			}
		});
	}

	private getFormValues() {
		this.userLogin.mail = this.mail.value;
		this.userLogin.password = this.password.value;
	}
}
