import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule, HttpClient} from '@angular/common/http';
import {SkillprofilePreviewComponent} from './preview/skillprofile-preview/skillprofile-preview.component';
import {ContactinfoComponent} from './preview/contactinfo/contactinfo.component';
import {ProjectinfoComponent} from './preview/projectinfo/projectinfo.component';
import {KeyStrengthsinfoComponent} from './preview/key-strengthsinfo/key-strengthsinfo.component';
import {SelfAssessmentScaleComponent} from './self-assessment-scale/self-assessment-scale.component';
import {TechnologyMethodeinfoComponent} from './preview/technology-methodeinfo/technology-methodeinfo.component';
import {SkillprofileEditComponent} from './edit/skillprofile-edit/skillprofile-edit.component';
import {ContactinfoEditComponent} from './edit/contactinfo-edit/contactinfo-edit.component';
import {KeyStrengthsinfoEditComponent} from './edit/key-strengthsinfo-edit/key-strengthsinfo-edit.component';
import {ProjectinfoEditComponent} from './edit/projectinfo-edit/projectinfo-edit.component';
import {TechnologyMethodeinfoEditComponent} from './edit/technology-methodeinfo-edit/technology-methodeinfo-edit.component';
import {FooterComponent} from './footer/footer.component';
import {HeaderComponent} from './header/header.component';
import {HomepageComponent} from './homepage/homepage.component';
import {LoginComponent} from './login/login.component';
import {RegistrationComponent} from './registration/registration.component';
import {ProfileSearchComponent} from './profile-search/profile-search.component';
import {ProfilePreviewComponent} from './profile-preview/profile-preview.component';
import {EducationComponent} from './preview/education/education.component';
import {EducationEditComponent} from './edit/education-edit/education-edit.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

// Angular Material
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSliderModule} from '@angular/material/slider';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { RoleChangerComponent } from './role-changer/role-changer.component';

@NgModule({
	declarations: [
		AppComponent,
		FooterComponent,
		HeaderComponent,
		HomepageComponent,
		LoginComponent,
		RegistrationComponent,
		ProfileSearchComponent,
		ProfilePreviewComponent,
		SkillprofilePreviewComponent,
		ContactinfoComponent,
		ProjectinfoComponent,
		KeyStrengthsinfoComponent,
		SelfAssessmentScaleComponent,
		TechnologyMethodeinfoComponent,
		SkillprofileEditComponent,
		ContactinfoEditComponent,
		KeyStrengthsinfoEditComponent,
		ProjectinfoEditComponent,
		TechnologyMethodeinfoEditComponent,
		EducationComponent,
		EducationEditComponent,
  RoleChangerComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		FormsModule,
		ReactiveFormsModule,
		HttpClientModule,
		BrowserAnimationsModule,
		MatTooltipModule,
		MatFormFieldModule,
		MatInputModule,
		MatSliderModule,
		NgbModule
	],
	exports: [MatFormFieldModule, MatInputModule, MatSliderModule],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {}
