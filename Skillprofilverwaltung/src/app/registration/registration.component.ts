import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LocalStorageService } from '../local-storage.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  userRegister = {
    username: "",
    firstname: "",
    surname: "",
    pwd: "",
    birthday: ""
  };

  registerForm!: FormGroup;

  constructor(
    private router: Router,
    private apiService: ApiService,
    private localStorageService: LocalStorageService
  ) { }

  ngOnInit(): void {
    this.registerForm = new FormGroup({
      mail: new FormControl(this.userRegister.username, [
        Validators.required,
        Validators.email
      ]),
      firstname: new FormControl(this.userRegister.firstname, [
        Validators.required,
        Validators.pattern(new RegExp("^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z])$")) // Nur Buchstaben (und Sonderzeichen, die direkt am Namen hängen)
      ]),
      surname: new FormControl(this.userRegister.surname,[
        Validators.required,
        Validators.pattern(new RegExp("^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z])$")) // Nur Buchstaben (und Sonderzeichen, die direkt am Namen hängen)
      ]),
      birthday: new FormControl(this.userRegister.birthday,
        Validators.required
        ),
      password: new FormControl(this.userRegister.pwd, [
        Validators.required,
        Validators.minLength(8),
        Validators.pattern(new RegExp("")) // Mind. 1 Zeichen/Buchstabe(groß und klein)/Sonderzeichen ^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$
      ]),
      password2: new FormControl(null)
    });

  }

  // Getter für die Registerform Controls
  get mail() { return this.registerForm.get('mail')!; }
  get firstname() { return this.registerForm.get('firstname')!; }
  get surname() { return this.registerForm.get('surname')!; }
  get birthday() { return this.registerForm.get('birthday')!; }
  get password() { return this.registerForm.get('password')!; }
  get password2() { return this.registerForm.get('password2')!; }


  onSubmit(): void {
    this.getFormValues();
    this.apiService.registerUser(this.userRegister)
      .subscribe({
        complete: () => { console.log("User registriert.") },
        error: (error) => {
          if (error.status !== undefined && error.status === 401) {
            console.log('Ungültige Werte');
          } else {
            console.log('different error code');
          }
        },
        next: (response) => {
          this.localStorageService.set('token', response.token);
          this.router.navigate(['/skillprofileEdit'])
        }
      });
  }

  private getFormValues() {
    this.userRegister.username = this.mail.value;
    this.userRegister.firstname = this.firstname.value;
    this.userRegister.surname = this.surname.value;
    this.userRegister.pwd = this.password.value;
    this.userRegister.birthday = this.birthday.value;
  }
}
