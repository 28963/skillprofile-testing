import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {

    constructor(
        private apiService: ApiService,
        private router: Router,
    ) { }

    ngOnInit(): void {
    }

    logout(): void {
        this.apiService.logout()
        this.router.navigate(['/homepage'])
    }
}
