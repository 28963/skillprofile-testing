import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {HomepageComponent} from './homepage/homepage.component';
import {LoginComponent} from './login/login.component';
import {ProfileSearchComponent} from './profile-search/profile-search.component';
import {RegistrationComponent} from './registration/registration.component';
import {SkillprofilePreviewComponent} from './preview/skillprofile-preview/skillprofile-preview.component';
import {SkillprofileEditComponent} from './edit/skillprofile-edit/skillprofile-edit.component';
import { RoleChangerComponent } from './role-changer/role-changer.component';

const routes: Routes = [
	{path: 'homepage', component: HomepageComponent},
	{path: 'login', component: LoginComponent},
	{path: 'registration', component: RegistrationComponent},
	{path: 'skillprofilePreview/:username', component: SkillprofilePreviewComponent},
	{path: 'skillprofileEdit', component: SkillprofileEditComponent},
	{path: 'profileSearch', component: ProfileSearchComponent},
	{path: 'roleChanger', component: RoleChangerComponent},
	{path: '', redirectTo: '/homepage', pathMatch: 'full'}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {}
