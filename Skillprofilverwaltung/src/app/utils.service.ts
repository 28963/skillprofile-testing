import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor() { }

  formatDate(dateString: string): string {
    let date = new Date(dateString)
    let dd = date.getUTCDate();
    let mm = date.getUTCMonth();
    let yy = date.getUTCFullYear();
    return dd + "." + mm + "." + yy;
  }
}
