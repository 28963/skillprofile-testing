import {ApiService} from './../api.service';
import {Component, Input, OnInit} from '@angular/core';
import {SkillProfile} from 'src/assets/skillprofile';
import {Skill} from 'src/assets/skill';

@Component({
	selector: 'app-profile-preview',
	templateUrl: './profile-preview.component.html',
	styleUrls: ['./profile-preview.component.css']
})
export class ProfilePreviewComponent implements OnInit {
	// Data will be inserted from profileSearch
	@Input() profilePreviewData!: SkillProfile;

	// Array with skill rating values to render Components and visualize the skills rating
	ratingPoints: number[] = [1, 2, 3, 4, 5];

	// Array with skills (all in the beginning, then top 5)
	skillsArray!: Skill[];

	constructor(private apiService: ApiService) {}

	ngOnInit(): void {
		this.skillsArray = this.generateTopSkillsList(this.profilePreviewData.skillprofile.skills);
	}

	// Sorts the array of skills for their rating value (descending)
	// And the end the array will be slice to maximum 5 objects
	generateTopSkillsList(skillsArray: Skill[]): Skill[] {
		skillsArray.sort((skillA: Skill, skillB: Skill) => {
			const skillRatingA = skillA.rating;
			const skillRatingB = skillB.rating;
			if (skillRatingA < skillRatingB) {
				return 1;
			}
			if (skillRatingA > skillRatingB) {
				return -1;
			}

			// If the values are equal the order won't be changed
			return 0;
		});
		// Picks the top five skills, we dont want to display more than five on the preview page
		return skillsArray.slice(0, 5);
	}
}
