import {Component, OnInit} from '@angular/core';
import {ACCOUNTS} from 'src/assets/accountData';
import {ApiService} from '../api.service';

@Component({
	selector: 'app-role-changer',
	templateUrl: './role-changer.component.html',
	styleUrls: ['./role-changer.component.css']
})
export class RoleChangerComponent implements OnInit {
	constructor(private apiService: ApiService) {}

	accounts!: any;
	roleChanges: string[] = [];

	roles: string[] = ['admin', 'pm', 'user'];

	ngOnInit(): void {
		this.getAccountInfos();
		
		/* Test Daten
		this.accounts = ACCOUNTS;
		for (let i = 0; i < this.accounts.length; i++) {
			this.roleChanges[i] = this.accounts[i].role;
		}
		*/
	}

	saveRoleChange(accountInfo: any, newRole: string, index: number): void {
		let newAccountInfo = accountInfo;
		newAccountInfo.role = newRole;
		this.apiService.editAccountInfo(newAccountInfo).subscribe({
			complete: () => {
				console.log('RoleChanger | put: successfull');
			},
			error: (error) => {
				if (error.status !== undefined && error.status === 401) {
					console.log('RoleChanger | put: Unauthorized | ' + error.message);
				} else {
					console.log('RoleChanger | put: error | ' + error.message);
				}
			},
			next: (response) => {
				this.accounts[index].role = newRole;
			}
		});
	}

	getAccountInfos(): void {
		this.apiService.getAllAcountinfos().subscribe({
			complete: () => {
				console.log('RoleChanger | get: successfull');
			},
			error: (error) => {
				if (error.status !== undefined && error.status === 401) {
					console.log('RoleChanger | get: Unauthorized | ' + error.message);
				} else {
					console.log('RoleChanger | get: error | ' + error.message);
				}
			},
			next: (response) => {
				this.accounts = response;

				for (let i = 0; i < this.accounts.length; i++) {
					this.roleChanges[i] = this.accounts[i].role;
				}
			}
		});
	}
}
