import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ProfilePreviewComponent } from '../profile-preview/profile-preview.component';

import { ProfileSearchComponent } from './profile-search.component';

describe('ProfileSearchComponent', () => {
	let component: ProfileSearchComponent;
	let fixture: ComponentFixture<ProfileSearchComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [ProfileSearchComponent],
		}).compileComponents();

		fixture = TestBed.createComponent(ProfileSearchComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
