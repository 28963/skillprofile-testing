import {ApiService} from 'src/app/api.service';
import {SkillProfile} from 'src/assets/skillprofile';
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormArray} from '@angular/forms';

@Component({
	selector: 'app-profile-search',
	templateUrl: './profile-search.component.html',
	styleUrls: ['./profile-search.component.css']
})
export class ProfileSearchComponent implements OnInit {
	profileList: SkillProfile[] = [];
	// Initializing the searchForm
	searchForm = this.fb.group({
		name: [''],
		job: [''],
		skills: this.fb.array([this.fb.control('')])
	});

	constructor(private apiService: ApiService, private fb: FormBuilder) {}

	ngOnInit(): void {
		// Request all skill profiles
		this.apiService.getAllSkillprofiles('').subscribe({
			complete: () => {
				console.log('Obtaining profile list complete');
			},
			error: (error) => {
				if (error.status !== undefined && error.status === 401) {
					console.log('Obtained profile list contains invalid values');
				} else {
					console.log('Something went wrong while obtaining the profile list');
				}
			},
			next: (response) => {
				console.log('Logging the profile list:');
				console.log(response);

				this.profileList = this.sortAlphabetically(response, false);
			}
		});

		this.updateProfileList();
	}

	// Sorts the profile list alphabetically and orders (ascending or descending) it depending on the given 'invert' value
	// If this function will be triggered by the user, there should be a way to set the invert value
	sortAlphabetically(profileArray: SkillProfile[], invert: boolean): SkillProfile[] {
		profileArray.sort((profileA: SkillProfile, profileB: SkillProfile) => {
			const nameA = profileA.accountInfo.firstname.toUpperCase();
			const nameB = profileB.accountInfo.firstname.toUpperCase();
			if (nameA < nameB) {
				return -1;
			}
			if (nameA > nameB) {
				return 1;
			}

			// If the values are equal the order won't be changed
			return 0;
		});
		if (invert) {
			profileArray.reverse();
		}
		return profileArray;
	}

	// checks all set filters and creates a query string
	updateProfileList() {
		this.searchForm.valueChanges.subscribe((data) => {
			console.log('searchForm value changed, new values:');
			console.log(data);

			let query = new URLSearchParams();
			if (data.job?.trim() != '') {
				query.append('job', data.job!.trim());
			}
			if (data.name?.trim() != '') {
				query.append('name', data.name!.trim());
			}
			if (data.skills![0] != '') {
				// query.append('job', data.name);
				console.log('skills sind auch da');
			}
			// Adding the '?' dynamically
			let queryString: string = query.toString();
			queryString = queryString == '' ? '' : `?${queryString}`;
			console.log('crafted queryString is: ');
			console.log(queryString);

			// Requests a filtered profile list and subscribes it
			this.apiService.getAllSkillprofiles(queryString).subscribe({
				complete: () => {
					console.log('Obtaining filtered profile list complete');
				},
				error: (error) => {
					if (error.status !== undefined && error.status === 401) {
						console.log('Obtained filtered profile list contains invalid values');
					} else {
						console.log('Something went wrong while obtaining the  filtered profile list');
					}
				},
				next: (response) => {
					console.log('Logging the filtered profile list:');
					console.log(response);
					this.profileList = this.sortAlphabetically(response, false);
				}
			});
		});
	}
}
