import { Component, Input, OnInit } from '@angular/core';
import { UtilsService } from 'src/app/utils.service';

@Component({
  selector: 'app-contactinfo',
  templateUrl: './contactinfo.component.html',
  styleUrls: ['./contactinfo.component.css']
})
export class ContactinfoComponent implements OnInit {

  @Input() info!: any;
  @Input() personalData!: any;

  constructor(public utilService: UtilsService) { }

  ngOnInit(): void {
  }

}
