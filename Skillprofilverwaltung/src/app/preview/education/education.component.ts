import { Component, Input, OnInit } from '@angular/core';
import { UtilsService } from 'src/app/utils.service';
import { Qualification } from 'src/assets/qualification';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.css']
})
export class EducationComponent implements OnInit {


  @Input() qualifications!: Qualification[];
  
  constructor(public utilService: UtilsService) { }

  ngOnInit(): void {
  }

}
