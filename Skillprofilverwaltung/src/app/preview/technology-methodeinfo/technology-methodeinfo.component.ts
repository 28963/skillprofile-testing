import { Component, Input, OnInit } from '@angular/core';
import { Skill } from 'src/assets/skill';

@Component({
  selector: 'app-technology-methodeinfo',
  templateUrl: './technology-methodeinfo.component.html',
  styleUrls: ['./technology-methodeinfo.component.css']
})
export class TechnologyMethodeinfoComponent implements OnInit {

  @Input() skills!: Skill[];

  constructor() { }

  ngOnInit(): void {
  }

}
