import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TechnologyMethodeinfoComponent } from './technology-methodeinfo.component';

describe('TechnologyMethodeinfoComponent', () => {
  let component: TechnologyMethodeinfoComponent;
  let fixture: ComponentFixture<TechnologyMethodeinfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TechnologyMethodeinfoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TechnologyMethodeinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
