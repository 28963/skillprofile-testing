import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KeyStrengthsinfoComponent } from './key-strengthsinfo.component';

describe('KeyStrengthsinfoComponent', () => {
  let component: KeyStrengthsinfoComponent;
  let fixture: ComponentFixture<KeyStrengthsinfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KeyStrengthsinfoComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(KeyStrengthsinfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
