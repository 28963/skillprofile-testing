import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-key-strengthsinfo',
  templateUrl: './key-strengthsinfo.component.html',
  styleUrls: ['./key-strengthsinfo.component.css']
})
export class KeyStrengthsinfoComponent implements OnInit {

  @Input() keyStrengths!: string[];

  constructor() { }

  ngOnInit(): void {
  }

}
