import {Component, OnInit} from '@angular/core';
import {ApiService} from 'src/app/api.service';
import {SkillProfile} from 'src/assets/skillprofile';
import {ActivatedRoute} from '@angular/router';
import {saveAs} from 'file-saver'

@Component({
	selector: 'app-skillprofile-preview',
	templateUrl: './skillprofile-preview.component.html',
	styleUrls: ['./skillprofile-preview.component.css']
})
export class SkillprofilePreviewComponent implements OnInit {
	constructor(private apiService: ApiService, private route: ActivatedRoute) {}

	ngOnInit(): void {
		const routeParameterUsername = this.route.snapshot.paramMap.get('username');
		let username: string = routeParameterUsername !== null ? routeParameterUsername : '';
		this.apiService.getSkillprofile(username!).subscribe({
			complete: () => {
				console.log('Profile obtained');
			},
			error: (error) => {
				if (error.status !== undefined && error.status === 401) {
					console.log('Ungültige Werte');
				} else {
					console.log('different error code');
				}
			},
			next: (response) => {
				console.log(response);
				this.profile = this.apiService.parseSkillProfile(response as unknown as string)[0];
			}
		});
	}

	profile!: SkillProfile;

	getPDF(): void {
		this.apiService.getPDF(this.profile.accountInfo.username).subscribe({
			complete: () => {
				console.log("PDF wurde erhalten.");
			},
			error: (e) => {
				// todo: Rechtezugriff (Nutzer darf nur eigenes Profil runterladen)
				console.log("PDF nicht erhalten!");
				console.log("Fehler: " + e.name);
				console.log("Fehler: " + e.status);
				console.log("Fehler: " + e.message);
			},
			next: (response) => {
				let fileName = this.profile.accountInfo.surname + "." + this.profile.accountInfo.firstname + "-skillprofile.pdf"
				saveAs(response, fileName)
			}
		});
	}
}
