import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillprofilePreviewComponent } from './skillprofile-preview.component';

describe('SkillprofilePreviewComponent', () => {
  let component: SkillprofilePreviewComponent;
  let fixture: ComponentFixture<SkillprofilePreviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SkillprofilePreviewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SkillprofilePreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
