import { Component, Input, OnInit } from '@angular/core';
import { UtilsService } from 'src/app/utils.service';
import { Project } from 'src/assets/project';

@Component({
  selector: 'app-projectinfo',
  templateUrl: './projectinfo.component.html',
  styleUrls: ['./projectinfo.component.css']
})
export class ProjectinfoComponent implements OnInit {

  @Input() projects!: Project[];

  constructor(public utilService: UtilsService) { }

  ngOnInit(): void {
  }

}
