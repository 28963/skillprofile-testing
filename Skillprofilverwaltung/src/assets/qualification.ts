export interface Qualification {
    start: string,
    end: string,
    info: string,
    place: string,
}

export const emptyQalificarion = () : Qualification => ({
    start: "",
    end: "",
    info: "",
    place: "",
})