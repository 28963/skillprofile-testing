FROM node:latest
WORKDIR /usr/src/app
COPY ./myapp .
RUN npm install
EXPOSE 3000
CMD ["npm", "start"]